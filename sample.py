class Sample:
    def __init__(self, id_s, genes, label):
        self.id_s = id_s
        self.genes = genes
        self.label = label
        for gene in genes:
            int(gene)

    def compute_euclidean_distance(self, other):
        sum_dist = 0
        for i in range(0, len(self.genes)):
            sum_dist += (self.genes[i] - other.genes[i]) ** 2
        return sum_dist ** 0.5

    def __str__(self):
        return str(self.id_s)
