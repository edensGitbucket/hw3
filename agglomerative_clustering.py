from cluster import Cluster
import sys


class AgglomerativeClustering:
    def __init__(self, link, samples):
        self.link = link
        self.samples = samples

    def create_clusters(self):
        clusters = []
        for sample in self.samples:
            clusters.append(Cluster(sample.id_s, sample))
        clusters.sort()
        return clusters

    def run(self, max_clusters):
        print(self.link)
        clusters = self.create_clusters()
        min_distance_clusters = ()
        while len(clusters) > max_clusters:
            min_distance = sys.maxsize
            for cluster in clusters:
                for second_cluster in clusters:
                    if cluster == second_cluster:
                        break
                    current_distance = self.link.compute(cluster, second_cluster)
                    if current_distance < min_distance:
                        min_distance = current_distance
                        min_distance_clusters = (second_cluster, cluster)
            min_distance_clusters[0].merge(min_distance_clusters[1])
            remove_id = min_distance_clusters[1].c_id
            for cluster in clusters:
                if cluster.c_id == remove_id:
                    clusters.remove(cluster)
            print('{} clusters remaining'.format(len(clusters)))
        for cluster in clusters:
            print(cluster)
