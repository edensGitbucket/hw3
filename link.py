import sys
from abc import abstractmethod


class Link:
    @abstractmethod
    def compute(self, cluster, other, distances=None):
        pass

    @abstractmethod
    def __str__(self):
        pass


class SingleLink(Link):
    def compute(self, cluster, other, distances=None):
        min_distance = sys.maxsize
        for sample in cluster.samples:
            for second_sample in other.samples:
                min_distance = min(sample.compute_euclidean_distance(second_sample), min_distance)
        return min_distance

    def __str__(self):
        return "Single Link:"


class CompleteLink(Link):
    def compute(self, cluster, other, distances=None):
        max_distance = 0
        for sample in cluster.samples:
            for second_sample in other.samples:
                max_distance = max(sample.compute_euclidean_distance(second_sample), max_distance)
        return max_distance

    def __str__(self):
        return "Complete Link:"
