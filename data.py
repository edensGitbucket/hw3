import pandas
from sample import Sample


class Data:
    def __init__(self, path):
        df = pandas.read_csv(path)
        self.data = df.to_dict(orient="list")

    def create_samples(self):
        samples = []
        num_of_samples = len(self.data['sample'])
        for sample in range(num_of_samples):
            genes_list_sample = []
            for key in self.data:
                genes_list_sample.append(self.data[key][sample])
            genes_list_sample.pop(0)  # the first sample is the string sample#number
            label = genes_list_sample[-1]
            genes_list_sample.pop(-1)
            samples.append(Sample(sample, genes_list_sample, label))
        return set(samples)

