class Cluster:
    def __init__(self, c_id, samples):
        self.c_id = c_id
        self.samples = set()
        self.samples.add(samples)

    def merge(self, other):
        print('merged clusters {} and {}'.format(self.c_id, other.c_id))
        # min_id = min(self.c_id, other.c_id)
        # self.c_id = min_id
        for sample in other.samples:
            self.samples.add(sample)

    def dominant_label(self):
        labels = {"PRAD": 0, "LUAD": 0, "COAD": 0, "KIRC": 0, "BRCA": 0}
        for sample in self.samples:
            for label in labels:
                if sample.label == label:
                    labels[label] += 1
        max_label = "BRCA"
        for label in labels:
            if labels[label] > labels[max_label]:
                max_label = label
        return max_label, labels[max_label]

    def compute_purity(self):
        return self.dominant_label()[1] / len(self.samples)

    def __str__(self):
        formatted_samples = sorted([sample.id_s for sample in self.samples])
        return 'Cluster {}: {}, dominant label: {}, purity: {}'.format(self.c_id, formatted_samples,
                                                                       self.dominant_label()[0], self.compute_purity())

    def __lt__(self, other):  # <
        if self.c_id < other.c_id:
            return True
        return False
