import sys
from data import Data
from agglomerative_clustering import AgglomerativeClustering
import link


def main(argv):
    data = Data(argv[1])
    samples = data.create_samples()
    links = list(argv[2].split(", "))
    if "Single_Link" in links:
        linker = link.SingleLink()
        runner = AgglomerativeClustering(linker, samples)
        runner.run(int(argv[3]))
    if "Complete_Link" in links:
        linker = link.CompleteLink()
        runner = AgglomerativeClustering(linker, samples)
        runner.run(int(argv[3]))


if __name__ == "__main__":
    main(sys.argv)
